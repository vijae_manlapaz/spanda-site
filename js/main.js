$(function(){
	// CONTENT 3
	$('.content3__items').slick({
		arrows: false,
		dots: false,
		slidesToShow: 3,
		responsive: [
			{
				breakpoint: 769,
				settings:{
					slidesToShow: 1,
					dots: true,
					fade: true
				}
			}
		]
	});

	// HEADER
	$('.header1__items').slick({
		arrows: false,
		dots: false,
		slidesToShow: 3,
		responsive: [
			{
				breakpoint: 769,
				settings:{
					slidesToShow: 1,
					dots: true,
					fade: true
				}
			}
		]
	});
	// HEADER
	$('.header__menuIcon').on('click', function(){
		$(this).toggleClass('header__menuIcon-active');
		$('.menu').toggleClass('menu-show');
		$('body').toggleClass('body-disabled');
		return false;
	});
	$(window).on('scroll resize', function(){
		var position = $(window).scrollTop();
		console.log(position);
		if(position > 100){
			$('.header__head').addClass('header__head-active');
		}else{
			$('.header__head').removeClass('header__head-active');
		}
	})
	// CONTACT
	$('#contact__page2__link').on('click', function(){
		$('.contact .container').addClass('hide');
		$('#contact__page2').removeClass('hide');
		return false;
	});
	$('#contact__page1__link').on('click', function(){
		$('.contact .container').addClass('hide');
		$('#contact__page1').removeClass('hide');
		return false;
	});
})
